from typing import List

from discord import Embed
from discord.ext.commands import Context

from store.entities.person import Person
from utils.context import get_nickname

RICARDO_GIF = "https://media1.tenor.com/images/f8ef221a5ec79435cdf42308a68cd261/tenor.gif"


def get_top_pp_embed(ctx: Context, persons: List[Person]) -> Embed:
    embed = Embed(
        title="Таблица лидеров PP :arrow_up:",
        color=0x00a0ea,
        description="Самые **БОЛЬШИЕ** :eggplant:",
    )
    for p in persons:
        embed.add_field(
            name=get_nickname(ctx, p.discord_name),
            value="%d см" % p.peepee,
            inline=False
        )
    embed.set_footer(
        text="Oh yeah!",
        icon_url=RICARDO_GIF
    )
    return embed
