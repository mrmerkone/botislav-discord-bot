from discord import Embed

from integrations.opendota.wrapers import OpenDotaUser

RANK_EMBED_COLOR = 0x00a0ea
RANK_TITLE_TEMPLATE = "**{}**"
RANK_DESCRIPTION_TEMPLATE = "**Ранг**: {}\n" \
                            "**Estimated solo mmr**: {}"


def get_rank_embed(user: OpenDotaUser) -> Embed:
    embed = Embed(
        title=RANK_TITLE_TEMPLATE.format(user.name),
        description=RANK_DESCRIPTION_TEMPLATE.format(user.rank_tier, user.mmr_estimate),
        color=RANK_EMBED_COLOR
    )
    embed.set_thumbnail(url=user.avatar_url)
    return embed
