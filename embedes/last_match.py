from typing import Dict, Any

from discord import Embed
from discord.ext.commands import Context

from integrations.opendota.wrapers import OpenDotaUser, MatchParticipant
from constants.emotes import MumblachEmotes
from utils.context import get_emoji

LAST_MATCH_EMBED_COLOR = 0x00a0ea
LAST_MATCH_TITLE_TEMPLATE = "{start_date} {game_mode} {reaction}"
LAST_MATCH_DESCRIPTION_TEMPLATE = "**{username}** {win_or_lose_verb} на\n" \
                                  "**{hero_name}** со счетом {score}\n\n" \
                                  "{damage_emote}**Урон по героям**: {hero_damage}\n" \
                                  "{gold_emote}**Общая ценность**: {total_gold}\n\n" \
                                  "Посмотреть на [OpenDota]({match_url})"


def _get_title_template_context(ctx: Context, user: OpenDotaUser) -> Dict[str, Any]:
    participant = user.lastmatch.get_participant(user.account_id)
    reaction = get_emoji(ctx, MumblachEmotes.FEELS_GOOD) if participant.win else get_emoji(ctx, MumblachEmotes.SADGE)
    return {
        "start_date": user.lastmatch.start_date,
        "game_mode": user.lastmatch.game_mode,
        "reaction": reaction
    }


def _get_description_template_context(ctx: Context, user: OpenDotaUser) -> Dict[str, Any]:
    participant = user.lastmatch.get_participant(user.account_id)
    return {
        "username": user.name,
        "win_or_lose_verb": _win_or_lose_verb(participant),
        "hero_name": participant.hero.localized_name,
        "score":  participant.score,
        "damage_emote": get_emoji(ctx, MumblachEmotes.ATTACK),
        "hero_damage": participant.hero_damage,
        "gold_emote": get_emoji(ctx, MumblachEmotes.GOLD),
        "total_gold": participant.total_gold,
        "match_url": user.lastmatch.url

    }


def _win_or_lose_verb(participant: MatchParticipant) -> str:

    if participant.deaths == 0:
        return "отыграл без ошибок"

    if participant.win:
        if participant.kda <= 1:
            return "старался слить, но не смог"
        if 1 < participant.kda <= 1.5:
            return "был якорем"
        if 1.5 < participant.kda <= 2:
            return "играл с бустером"
        if 2 < participant.kda <= 3:
            return "жал кнопки вовремя"
        if 3 < participant.kda <= 4:
            return "затащил"
        if participant.kda > 4:
            return "залил соляры"
    else:
        if participant.kda > 4:
            return "проиграл 1vs9"
        if 3 < participant.kda <= 4:
            return "очень обидно проиграл"
        if 2 < participant.kda <= 3:
            return "пытался победить"
        if 1.5 < participant.kda <= 2:
            return "закинул"
        if 1 < participant.kda <= 1.5:
            return "не понимал, что происходит"
        if participant.kda <= 1:
            return "стоял АФК"


def get_lastmatch_embed(ctx: Context, user: OpenDotaUser) -> Embed:
    participant = user.lastmatch.get_participant(user.account_id)
    title_context = _get_title_template_context(ctx, user)
    description_context = _get_description_template_context(ctx, user)
    embed = Embed(
        title=LAST_MATCH_TITLE_TEMPLATE.format(**title_context),
        description=LAST_MATCH_DESCRIPTION_TEMPLATE.format(**description_context),
        color=LAST_MATCH_EMBED_COLOR
    )
    embed.set_thumbnail(url=participant.hero.image_vert_url)
    return embed
