import os

try:
    from conf import dev_env_setup
    dev_env_setup.setup_env()
except ImportError:
    pass


class Discord:
    DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')


class Telegram:
    TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN')
    TELEGRAM_CHAT_ID = os.getenv('TELEGRAM_CHAT_ID')
    TELEGRAM_SUMMON_MENTIONS = os.getenv('TELEGRAM_SUMMON_MENTIONS')


class Database:
    DB_HOST = os.getenv('DB_HOST')
    DB_NAME = os.getenv('DB_NAME')
    DB_USER = os.getenv('DB_USER')
    DB_PASS = os.getenv('DB_PASS')
