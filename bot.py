import sys
import logging

from discord.ext.commands import Bot

import store
import commands
import listeners
from conf import env

logger = logging.getLogger(__name__)


def setup_logging():
    logging_format = "[%(asctime)s] [%(levelname)s] %(name)s: %(message)s"
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=logging_format)


def setup_database():
    logger.info("Initializing database connection")
    store.init()


def setup_bot():
    logger.info("Initializing discord bot")

    # TODO перейти на Cogs в будующем
    # https://discordpy.readthedocs.io/en/latest/ext/commands/cogs.html#ext-commands-cogs

    bot = Bot(command_prefix="!", description="Botislav bot")

    logger.info("Registering commands")
    for command in map(commands.__dict__.get, commands.__all__):
        bot.add_command(command)
        logger.info(f"Registered \"{command.name}\" command")

    logger.info("Registering listeners")
    for listener in map(listeners.__dict__.get, listeners.__all__):
        bot.add_listener(listener, listener.event)
        logger.info(f"Registered \"{listener.__name__}\" listener on \"{listener.event}\" event")

    bot.run(env.Discord.DISCORD_TOKEN)


if __name__ == "__main__":
    setup_logging()
    setup_database()
    setup_bot()

