__all__ = [
    "NonExistentProfile"
]


class NonExistentProfile(Exception):
    pass
