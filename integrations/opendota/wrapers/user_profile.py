import json
import requests
import functools

from typing import Dict, Any

from integrations.opendota.exceptions import NonExistentProfile
from integrations.opendota.wrapers import Match
from utils.common import int_to_roman

DOTA_RANKS = [
    'Herald',
    'Guardian',
    'Crusader',
    'Archon',
    'Legend',
    'Ancient',
    'Divine',
    'Immortal'
]


class OpenDotaUser:
    """
    Represents open dota user.
    """
    TIMEOUT = 5

    _data: Dict[str, Any]

    def __init__(self, account_id: int):
        url = "https://api.opendota.com/api/players/{}".format(account_id)
        r = requests.get(url, timeout=self.TIMEOUT)
        if r.status_code == 404:
            raise NonExistentProfile
        self._data = json.loads(r.text)

    @property
    def name(self) -> str:
        return self._data.get("profile").get("personaname")

    @property
    def account_id(self) -> int:
        return self._data.get("profile").get("account_id")

    @property
    def rank_tier(self) -> str:
        rank_tier = self._data.get("rank_tier")
        rank = DOTA_RANKS[(rank_tier // 10) - 1]
        tier = int_to_roman(rank_tier % 10)
        return "{} {}".format(rank, tier)

    @property
    def mmr_estimate(self) -> str:
        return self._data.get("mmr_estimate").get("estimate")

    @property
    def steam_profile_url(self) -> str:
        return self._data.get("profile").get("profileurl")

    @property
    def avatar_url(self) -> str:
        return self._data.get("profile").get("avatarfull")

    @functools.cached_property
    def lastmatch(self) -> Match:
        url = "https://api.opendota.com/api/players/{}/matches?significant=0&limit=1".format(self.account_id)
        r = requests.get(url, timeout=self.TIMEOUT)
        match_id = json.loads(r.text)[0].get('match_id')
        return Match(match_id)
