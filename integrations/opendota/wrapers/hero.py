import json
import requests

from typing import Dict, List, Any


class Hero:
    """
    Represents hero object from OpenDota API.
    """
    HEROES_URL = "https://api.opendota.com/api/heroes"
    IMAGES_URL = "https://steamcdn-a.akamaihd.net/apps/dota2/images/heroes"

    _data: Dict[str, Any]

    def __init__(self, hero_id: int):
        r = requests.get(self.HEROES_URL)
        heroes = json.loads(r.text)
        for hero in heroes:
            if hero["id"] == hero_id:
                self._data = hero

    @property
    def name(self) -> str:
        return self._data["name"][14:]

    @property
    def localized_name(self) -> str:
        return self._data["localized_name"]

    @property
    def roles(self) -> List[str]:
        return self._data["roles"]

    @property
    def image_sb_url(self) -> str:
        return self.IMAGES_URL + "/{}_sb.png".format(self.name)

    @property
    def image_vert_url(self) -> str:
        return self.IMAGES_URL + "/{}_vert.jpg".format(self.name)

    @property
    def icon_url(self) -> str:
        return self.IMAGES_URL + "/{}_icon.png".format(self.name)
