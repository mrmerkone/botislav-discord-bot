import json
import requests
import datetime
import functools

from typing import Dict, Any, Optional

from integrations.opendota.wrapers import Hero

GAME_MODES = [
    "Unknown",
    "All pick",
    "Captains mode",
    "Random draft",
    "Single draft",
    "All random",
    "Intro",
    "Diretide",
    "Reverse captains mode",
    "Greeviling",
    "Tutorial",
    "Mid only",
    "Least played",
    "Limited heroes",
    "Compendium matchmaking",
    "Custom",
    "Captains draft",
    "Balanced draft",
    "Ability draft",
    "Event",
    "All random death match",
    "1v1 mid",
    "All draft",
    "Turbo",
    "Mutation"
]


class MatchParticipant:
    """
    Represents match participant from Match object.
    """
    _data: Dict[str, Any]

    def __init__(self, data: Dict[str, Any]):
        self._data = data

    @property
    def kills(self) -> int:
        return self._data['kills']

    @property
    def deaths(self) -> int:
        return self._data['deaths']

    @property
    def assists(self) -> int:
        return self._data['assists']

    @functools.cached_property
    def kda(self) -> float:
        if self.deaths == 0:
            return self.kills + self.assists
        return (self.kills + self.assists) / self.deaths

    @functools.cached_property
    def score(self) -> str:
        return "{}/{}/{}".format(
            self.kills,
            self.deaths,
            self.assists
        )

    @functools.cached_property
    def total_gold(self) -> str:
        if self._data['total_gold'] < 1000:
            return "%d" % int(self._data['total_gold'])
        return "{:0.2f}k".format(int(self._data['total_gold']) / 1000)

    @functools.cached_property
    def tower_damage(self) -> str:
        if self._data['tower_damage'] < 1000:
            return "%d" % int(self._data['tower_damage'])
        return "{:0.2f}k".format(int(self._data['tower_damage']) / 1000)

    @functools.cached_property
    def hero_damage(self) -> str:
        if self._data['hero_damage'] < 1000:
            return "%d" % int(self._data['hero_damage'])
        return "{:0.2f}k".format(int(self._data['hero_damage']) / 1000)

    @property
    def gpm(self) -> float:
        return self._data['gold_per_min']

    @property
    def xpm(self) -> float:
        return self._data['xp_per_min']

    @property
    def win(self) -> bool:
        return bool(self._data['win'])

    @functools.cached_property
    def hero(self) -> Hero:
        return Hero(self._data['hero_id'])


class Match:
    """
    Represents match object from OpenDota API.
    """
    TIMEOUT = 5

    _url: str

    def __init__(self, match_id: int):
        self._url = "https://api.opendota.com/api/matches/{}".format(match_id)
        r = requests.get(self._url, timeout=self.TIMEOUT)
        self._data = json.loads(r.text)

    @property
    def match_id(self) -> str:
        return self._data.get("match_id")

    @property
    def url(self) -> str:
        return "https://www.opendota.com/matches/{}".format(self.match_id)

    @functools.cached_property
    def duration(self) -> str:
        seconds = self._data["duration"]
        m = seconds // 60
        s = seconds - (m * 60)
        return "{:02d}:{:02d}".format(m, s)

    @property
    def game_mode(self) -> str:
        return GAME_MODES[self._data["game_mode"]]

    @functools.cached_property
    def start_date(self) -> str:
        return datetime.datetime.fromtimestamp(self._data["start_time"]).strftime('%d-%b-%Y')

    @functools.lru_cache(typed=True)
    def get_participant(self, account_id: int) -> Optional[MatchParticipant]:
        for player_data in self._data["players"]:
            if player_data.get("account_id") == account_id:
                return MatchParticipant(player_data)
        return None
