
DOZENS = [
    1000, 900, 500, 400,
    100, 90, 50, 40,
    10, 9, 5, 4,
    1
]


SYMBOLS = [
    "M", "CM", "D", "CD",
    "C", "XC", "L", "XL",
    "X", "IX", "V", "IV",
    "I"
]


def int_to_roman(num: int) -> str:
    """
    Converts an integer to a roman numeral.
    """
    roman_num = ''
    i = 0
    while num > 0:
        for _ in range(num // DOZENS[i]):
            roman_num += SYMBOLS[i]
            num -= DOZENS[i]
        i += 1
    return roman_num
