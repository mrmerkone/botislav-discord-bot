from discord.ext.commands import Context
from discord.utils import get


def get_username(ctx: Context):
    """
    Returns discord username as str.
    """
    return "{0.name}#{0.discriminator}".format(ctx.author)


def get_mention(ctx: Context):
    """
    Returns discord user mention as str.
    """
    return '{0.mention}'.format(ctx.author)


def get_emoji(ctx: Context, name: str):
    """
    Returns str emoji <:emoji_name:emoji_id> from client by its lookup.
    """
    return get(ctx.bot.emojis, name=name)


def get_nickname(ctx, username):
    """
    Returns nickname for provided username.
    """
    member = ctx.guild.get_member_named(username)
    if not member or not member.nick:
        return username.split("#")[0]
    return member.nick

