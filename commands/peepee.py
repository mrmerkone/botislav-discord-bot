import random

from discord.ext import commands
from discord.ext.commands import Context

from store.entities import PersonStorage
from embedes import get_top_pp_embed
from constants.responds import PEEPEE_PHRASES
from utils.context import get_username, get_mention

TOP = 'top'
TOP_LIMIT = 3


def _pp_change():
    rand_cases = [-2, -1, 0, 1, 2]
    probability = [0.1, 0.2, 0.4, 0.2, 0.1]
    return random.choices(rand_cases, probability)[0]


@commands.command(pass_context=True, aliases=['pp', 'mypp', 'mypeepee'])
@commands.cooldown(3, 60, commands.BucketType.user)
async def peepee(ctx: Context, args: str = ''):
    """
    Gets peepee size of person, it can grow or shrink every time you ask. WHOS GOT THE BIGGEST PEEPEE?
    """

    if args.lower() == TOP:
        persons = PersonStorage.get_top_pp_persons(TOP_LIMIT)
        embed = get_top_pp_embed(ctx, persons)
        return await ctx.send(embed=embed)

    person = PersonStorage.get(discord_name=get_username(ctx))

    if (not person) or (not person.peepee):
        pp_size = random.randint(20, 30)
        PersonStorage.update_or_create(
            discord_name=get_username(ctx),
            peepee=pp_size
        )
        msg = '{} Я тебя не знаю!\nДавай-ка измерим твоего друга. Ого он аж {} см'.format(
            get_mention(ctx),
            pp_size
        )
        return await ctx.send(msg)

    pp_delta = _pp_change()
    pp_length = person.peepee + pp_delta
    PersonStorage.update_or_create(
        discord_name=get_username(ctx),
        peepee=pp_length
    )

    msg = '{0} {1}. {2} см'.format(
        get_mention(ctx),
        PEEPEE_PHRASES.get(pp_delta),
        pp_length
    )
    await ctx.send(msg)


