from telegram import Bot
from discord.ext import commands

from conf.env import Telegram
from constants.emotes import MumblachEmotes
from utils.context import get_nickname, get_mention, get_emoji, get_username


@commands.command(pass_context=True)
@commands.cooldown(1, 360, commands.BucketType.channel)
async def summon(ctx):
    """
    Summons old mans to channel.
    """

    telegram_bot = Bot(token=Telegram.TELEGRAM_TOKEN)

    text = '{} {} вызывает олдов в mumblach'.format(
        Telegram.TELEGRAM_SUMMON_MENTIONS,
        get_nickname(ctx, get_username(ctx))
    )
    telegram_bot.send_message(
        chat_id=Telegram.TELEGRAM_CHAT_ID,
        text=text
    )

    msg = '{} суммоню олдов {}'.format(
        get_mention(ctx),
        get_emoji(ctx, MumblachEmotes.ZDAROVA)
    )
    return await ctx.send(msg)
