from discord.ext import commands
from discord.ext.commands import Context
from requests.exceptions import ReadTimeout

from store.entities import PersonStorage
from constants.emotes import MumblachEmotes
from integrations.opendota.wrapers import OpenDotaUser
from embedes import get_rank_embed
from utils.context import get_emoji, get_username


@commands.command(pass_context=True)
async def rank(ctx: Context):
    """
    Sends your estimated rank from OpenDota.
    """

    person = PersonStorage.get(discord_name=get_username(ctx))

    if (not person) or (not person.open_dota_id):
        msg = '{} Ты кто такой? Я тебя не знаю {}\nВоспользуйся `!addopendota`'.format(
            ctx.author.mention,
            get_emoji(ctx, MumblachEmotes.BULDIGA)
        )
        return await ctx.send(msg)

    try:
        user = OpenDotaUser(person.open_dota_id)
        embed = get_rank_embed(user)
    except ReadTimeout:
        # send respond if OpenDota not avaliable
        msg = '{} OpenDota лежит и не встает {}'.format(
            ctx.author.mention,
            get_emoji(ctx, MumblachEmotes.POMINKI)
        )
        return await ctx.send(msg)

    # send respond
    msg = '{}'.format(
        ctx.author.mention
    )
    return await ctx.send(msg, embed=embed)
