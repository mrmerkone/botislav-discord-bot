import random
from discord.ext import commands

from constants.responds import EIGHT_BALL_PHRASES
from utils.context import get_mention


@commands.command(pass_context=True, aliases=['8ball', '8all', '8'])
async def eight_ball(ctx, args: str):
    """
    Asks the Bot for his random opinion.
    """

    if not args:
        text = "Что бы ты хотел меня спросить?"
    else:
        text = random.choice(EIGHT_BALL_PHRASES)

    msg = "{} {}".format(
        get_mention(ctx),
        text
    )
    await ctx.send(msg)
