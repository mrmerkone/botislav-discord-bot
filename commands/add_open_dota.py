import re
from typing import Optional

from discord.ext import commands
from discord.ext.commands import Context
from requests.exceptions import ReadTimeout

from store.entities import PersonStorage
from constants.emotes import MumblachEmotes
from integrations.opendota.wrapers import OpenDotaUser
from integrations.opendota.exceptions import NonExistentProfile
from utils.context import get_emoji, get_username, get_mention

PATTERN = re.compile(r'players\/(?P<account_id>\d+)\/?$')


def _get_account_id(url: str) -> Optional[int]:
    match = PATTERN.search(url)
    if not match:
        return None
    return int(match.groupdict().get('account_id'))


@commands.command(pass_context=True, aliases=['aod', 'addopendota'])
async def add_open_dota(ctx: Context, url: str):
    """
    Adds your open profile allowing you to use !lastmatch.
    """

    account_id = _get_account_id(url)

    try:
        user = OpenDotaUser(account_id)
    except NonExistentProfile:
        msg = '{} Не могу найти профиль по этой ссылке {}'.format(
            get_mention(ctx),
            get_emoji(ctx, MumblachEmotes.SADGE)
        )
        return await ctx.send(msg)
    except ReadTimeout:
        msg = '{} OpenDota лежит и не встает {}'.format(
            ctx.author.mention,
            get_emoji(ctx, MumblachEmotes.POMINKI)
        )
        return await ctx.send(msg)

    PersonStorage.update_or_create(
        discord_name=get_username(ctx),
        open_dota_id=account_id
    )

    msg = '{} привязал к тебе этот профиль {}\n{}'.format(
        get_mention(ctx),
        get_emoji(ctx, MumblachEmotes.EBALO),
        url
    )
    return await ctx.send(msg)

