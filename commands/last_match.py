from discord.ext import commands
from discord.ext.commands import Context
from requests.exceptions import ReadTimeout

from store.entities import PersonStorage
from constants.emotes import MumblachEmotes
from integrations.opendota.wrapers import OpenDotaUser
from embedes import get_lastmatch_embed
from utils.context import get_emoji, get_username, get_mention


@commands.command(pass_context=True, aliases=['lm', 'lastmatch'])
async def last_match(ctx: Context):
    """
    Sends your last match from OpenDota.
    """
    person = PersonStorage.get(discord_name=get_username(ctx))

    if (not person) or (not person.open_dota_id):
        msg = '{} Ты кто такой? Я тебя не знаю {}\nВоспользуйся `!addopendota`'.format(
            get_mention(ctx),
            get_emoji(ctx, MumblachEmotes.BULDIGA)
        )
        return await ctx.send(msg)

    try:
        user = OpenDotaUser(person.open_dota_id)
        embed = get_lastmatch_embed(ctx, user)
    except ReadTimeout:
        msg = '{} OpenDota лежит и не встает {}'.format(
            get_mention(ctx),
            get_emoji(ctx, MumblachEmotes.POMINKI)
        )
        return await ctx.send(msg)

    msg = '{}'.format(
        get_mention(ctx)
    )
    return await ctx.send(msg, embed=embed)
