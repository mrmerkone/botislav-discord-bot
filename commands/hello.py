from discord.ext import commands

from constants.emotes import MumblachEmotes
from utils.context import get_emoji, get_mention


@commands.command(pass_context=True)
async def hello(ctx):
    """Greets user."""

    # send respond
    msg = '{} Здарова {}'.format(
        get_mention(ctx),
        get_emoji(ctx, MumblachEmotes.ZDAROVA)
    )
    await ctx.send(msg)