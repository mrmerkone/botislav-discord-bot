# Botislav discord bot
[![pipeline status](https://gitlab.com/mrmerkone/botislav-discord-bot/badges/master/pipeline.svg)](https://gitlab.com/mrmerkone/botislav-discord-bot/commits/master)

## Startup
1. Install dependencies:
    * `$ pip install -r requirements.txt`
2. Setup your tokens and api keys in `config.py`
3. Run bot:
    * `$ python bot.py`

## Add bot

[+ add Botislav](https://discordapp.com/oauth2/authorize?client_id=415214530898034688&scope=bot)