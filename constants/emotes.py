__all__ = [
    "MumblachEmotes"
]


class MumblachEmotes:
    POMOIKA = "roflanPomoika"
    POMINKI = "roflanPominki"
    ZDAROVA = "roflanZdarova"
    GORIT = "roflanGorit"
    EBALO = "roflanEBALO"
    BULDIGA = "roflanBuldiga"
    SADGE = "sadge"
    GOLD = "gold"
    ATTACK = "attack"
    TEHE_PELO = "tehePelo"
    MISHA_SWAG = "MishaSWAG"
    FEELS_GOOD = "FeelsGood"
    KEKW = "kekw"
    KEKWUT = "kekwut"

