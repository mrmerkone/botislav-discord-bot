from typing import List

from pony.orm import desc, db_session, PrimaryKey, Optional

from store import db
from store.base import EntityStorage


class Person(db.Entity):
    """
    Stores discord person info.
    """
    discord_name = PrimaryKey(str)
    open_dota_id = Optional(int)
    peepee = Optional(int)


class PersonStorage(EntityStorage[Person]):
    """
    Manages Person instances.
    """
    entity = Person

    @classmethod
    @db_session
    def get_top_pp_persons(cls, limit: int) -> List[Person]:
        return cls.entity \
                  .select(lambda c: c.peepee is not None) \
                  .order_by(desc(cls.entity.peepee))[:limit]
