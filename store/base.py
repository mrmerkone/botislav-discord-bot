from typing import Optional, TypeVar, ClassVar, Generic

from pony.orm import db_session
from pony.orm.core import ObjectNotFound

T = TypeVar('T')


class EntityStorage(Generic[T]):
    """
    Provides basic methods to manipulate entities.
    """
    entity: ClassVar[T]

    @classmethod
    @db_session
    def exists(cls, **kwargs) -> bool:
        return bool(cls.get(**kwargs))

    @classmethod
    @db_session
    def create(cls, **kwargs) -> T:
        return cls.entity(**kwargs)

    @classmethod
    @db_session
    def get(cls, **kwargs) -> Optional[T]:
        # noinspection PyProtectedMember
        pk_name = cls.entity._pk_.name
        try:
            return cls.entity[kwargs.get(pk_name)]
        except ObjectNotFound:
            return None

    @classmethod
    @db_session
    def update(cls, **kwargs) -> Optional[T]:
        instance = cls.get(**kwargs)
        if instance:
            for name, value in kwargs.items():
                setattr(instance, name, value)
        return instance

    @classmethod
    @db_session
    def delete(cls, **kwargs):
        cls.get(**kwargs).delete()

    @classmethod
    @db_session
    def update_or_create(cls, **kwargs) -> T:
        if cls.exists(**kwargs):
            return cls.update(**kwargs)
        return cls.create(**kwargs)
