import logging
from pony import orm
from conf import env

logger = logging.getLogger(__name__)

__all__ = [
    'db', 'init'
]

db = orm.Database()

# initialize wrapers
# to avoid cyclic imports
import store.entities


def init(provider: str = 'postgres'):
    db.bind(
        provider=provider,
        user=env.Database.DB_USER,
        password=env.Database.DB_PASS,
        host=env.Database.DB_HOST,
        database=env.Database.DB_NAME
    )
    db.generate_mapping(create_tables=True)
    logger.info(f"Initialized database mapping. Host: \"{env.Database.DB_HOST}\" Provider: \"{provider}\"")
