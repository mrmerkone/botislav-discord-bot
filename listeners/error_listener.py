import logging

from discord.ext.commands import Context
from discord.ext import commands

from constants.emotes import MumblachEmotes
from utils.context import get_emoji, get_mention

logger = logging.getLogger(__name__)


async def error_listener(ctx: Context, exc: Exception):
    """Сommand error handler."""

    if isinstance(exc, commands.CommandOnCooldown):
        msg = "{} Команда на кулдауне еще {:.2f}с {}".format(
            get_mention(ctx),
            exc.retry_after,
            get_emoji(ctx, MumblachEmotes.GORIT)
        )
        await ctx.send(msg)

    elif isinstance(exc, commands.MissingRequiredArgument):
        msg = "{} Недостаточно аргументов. Воспользуйся ```!help <command_name>```".format(
            get_mention(ctx)
        )
        await ctx.send(msg)

    elif isinstance(exc, commands.CommandNotFound):
        msg = "{} Не знаю такой команды. Воспользуйся ```!help```".format(
            get_mention(ctx)
        )
        await ctx.send(msg)

    else:
        logger.error(exc)
        msg = 'Упc... Хьюстон, у нас проблемы... Требуются ремонтные работы для этой команды {}'.format(
            get_mention(ctx),
            get_emoji(ctx, MumblachEmotes.KEKWUT)
        )
        await ctx.send(msg)


error_listener.event = "on_command_error"
